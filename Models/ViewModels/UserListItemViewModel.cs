﻿namespace SampleSite.Models.ViewModels
{
    public class UserListItemViewModel
    {
        public UserListItemViewModel(int id, string userName, string email, int age, string address1, string address2, string city, string postCode)
        {
            this.Id = id;
            this.UserName = userName;
            this.Email = email;
            this.Age = age;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.PostCode = postCode;
        }

        public int Id { get; }
        public string UserName { get; }
        public string Email { get; }
        public int Age { get; }
        public string Address1 { get; }
        public string Address2 { get; }
        public string City { get; }
        public string PostCode { get; }
    }
}
