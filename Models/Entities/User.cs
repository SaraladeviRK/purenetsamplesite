﻿using System.ComponentModel.DataAnnotations;

namespace SampleSite.Models.Entities
{
    public class User : BaseEntity
    {
        [Required]
        public string UserName { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }
}
